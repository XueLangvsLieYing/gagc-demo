package com.gagc.discovery.eureka.client;

import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * create by lorne on 2018/1/26
 */
@Configuration
public class AlwaysSamplerConfiguration {


    @Bean
    public AlwaysSampler defaultSampler(){
        return new AlwaysSampler();
    }

}
