package com.gagc.discovery.eureka.client.controller;


import com.gagc.discovery.eureka.client.feign.ServerClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * create by lorne on 2018/1/26
 */
@RestController
public class ClientController {


    private Logger logger = LoggerFactory.getLogger(ClientController.class);


    @Autowired
    private ServerClient serverClient;


    @RequestMapping("/hello")
    public String hello(@RequestParam("name") String name){
        logger.info("consul-client,name:"+name);
        return "hello:"+serverClient.welcome(name);
    }

}
