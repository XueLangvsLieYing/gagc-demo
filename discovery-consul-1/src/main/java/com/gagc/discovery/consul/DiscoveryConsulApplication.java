package com.gagc.discovery.consul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class DiscoveryConsulApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscoveryConsulApplication.class, args);
	}
}
