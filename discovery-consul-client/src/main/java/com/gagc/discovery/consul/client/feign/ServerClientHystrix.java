package com.gagc.discovery.consul.client.feign;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * create by lorne on 2018/1/26
 */
@Component
public class ServerClientHystrix implements ServerClient{


    private Logger logger = LoggerFactory.getLogger(ServerClientHystrix.class);


    @Override
    public String welcome(String name) {
        logger.warn("welcome:"+name);
        return "no online:"+name;
    }
}
