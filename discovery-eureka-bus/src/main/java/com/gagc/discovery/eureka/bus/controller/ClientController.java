package com.gagc.discovery.eureka.bus.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * create by lorne on 2018/1/26
 */
@RefreshScope
@RestController
public class ClientController {

    @Value("${test.name}")
    private String name;


    @RequestMapping("/test")
    public String test(){
        return name;
    }

}
