# gagc-demo  
  
是基于springcloud的服务框架  
  

zookeeper consul ELK 搭建手册见wiki


包含的组件有：  
  
zookeeper consul eureka config bus feign hyxtrix  hyxtrix-server  turbine  zipkin-server  logback  sleuth  ELK monitor   
   
系统环境:  
centos7.2x64   
  

后面将添加 zuul lcn 服务



## 演示说明

服务器ip: 118.190.150.0   

关于代码的部署:   

代码通过jenkins将gitee的代码迁移下来自动编译并部署到服务器.   


已部署的服务:   
1.discovery-consul-1  (不对外)  
2.discovery-consul-2  (不对外)  
3.discovery-consul-client  8099  
4.discovery-zookeeper   (不对外)  
5.discovery-zookeeper-client  8081  
6.hystrix-server  9090  
7.turbine-consul  19090  
8.zipkin-server  9411  
9.zookeeper-demo  8199   
10.ELK(ElasticSearch Logstash Kibana)  5601   


consul 服务发现后台地址
http://118.190.150.0:8500/ui


consul 服务调用测试(演示负载均衡与feign hystrix):  
http://118.190.150.0:8099/hello?name=xiaohong   



响应结果:   
consul-hello:welcome-1:xiaohong   
consul-hello:welcome-2:xiaohong   

演示hystrix仪表盘   

打开 http://118.190.150.0:9090/hystrix 查看 hystrix-server，在地址栏中输入 http://118.190.150.0:19090/turbine.stream   
关于hystrix表盘说明 https://baijiahao.baidu.com/s?id=1587928659026817484&wfr=spider&for=pc   


演示zipkin调用链   
http://118.190.150.0:9411/zipkin/   
然后输入 http.path=/hello 查询找到最后新一次的数据，然后获取traceId 然后输入到kibana查找日志   


可通过traceId管理查看模块日志信息   
http://118.190.150.0:5601   



zookeeper 服务调用测试(演示负载均衡与feign hystrix):   
http://118.190.150.0:8081/hello?name=xiaohong   



zookeeper-demo 演示key的注册于时间机制

http://118.190.150.0:8199

设置key http://118.190.150.0:8199/set?name=hello

修改key http://118.190.150.0:8199/setVal?name=hello&val=test

删除key http://118.190.150.0:8199/del?name=hello

查看事件日志 http://118.190.150.0:8199/logs



## 使用spring cloud目前需要注意的问题及避规方法：

1. 服务如何划分，该不该划分   

      服务之间严禁出现的事情：   
      
      1.不同的模块操作相同的数据。    
      
      上了微服务以后很容易出现的问题就是把项目拆的特别细，任何模块皆服务。如何清晰的定义模块与服务的关系。   
      
      我们划分服务模块通常是按照以数据优先的方式来划分模块的，例如会把用户模块下所有相关联的表作为一个模块来控制。  
      
      我们定义了四层架构的方式去优化项目框架，四层框架可以更清楚的区分业务模块也能为我们模块之前的协调开发提供有力的手段。   
      
      http://mp.weixin.qq.com/s/b4Oz3R9axX5EczsiWCRa4A  
      
      不建议上来就要将项目拆分成多个服务，但是项目需要做到能拆.  
      
      http://mp.weixin.qq.com/s/X2af1SwsRYUQ_SFE1o02qg  
            

2. 数据库事务   

    单机时代事务都是完全依赖本地的数据库，微服务到来以后事务将不再简单。网上有很多方案TCC MQ消息中间件 二阶段 等等。各有千秋与利弊但是使用来说都比较繁琐，因此我们开发了LCN-分布式事务框架。一个协调性的事务框架，我们也将其开源到了github gitee 目前粉丝用户600+ 2017年被开源中国-码云评为80个最有价值的开源源码之一(GVP)。         
    项目技术上采用的是springcloud+netty 机制类似于 sleuth,在使用的是时候几乎与本地事务无差异。不增加额外的成本,我们在负载均衡方便做了更多的优化，性能可以与本地事务相媲美。  

3. 如何有效的运维与部署   

    部署方面：我们采用Jenkins自动化部署，然后定义了一套项目开发标准，只要按照我们的标准在项目部署上基本上不需要太多时间。   
    运维： 我们采用spring-monitor机制，当模块出现问题将会马上邮件或者短信电话通知运维,做到更即使的监控。   

4. 如何查看日志分析bug   

    日志我们采用sleuth + ELK +logback 收集日志，将多模块之间的日志串起来做到与单机模块日志查找一样方便与全面。   

5. 优化的feign的机制   
 
    由于对外请求时需要对异常做一些处理。feign在使用时无法与模块一样灵活的使用自定义异常。我们自己定义了一套feign访问与异常定义机制。目前代码也都开源在了github平台下.  
    



