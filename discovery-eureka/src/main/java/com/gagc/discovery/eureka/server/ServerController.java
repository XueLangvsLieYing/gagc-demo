package com.gagc.discovery.eureka.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * create by lorne on 2018/1/26
 */
@RestController
public class ServerController {




    private Logger logger = LoggerFactory.getLogger(ServerController.class);

    @RequestMapping(value = "/welcome",method = RequestMethod.GET)
    public String welcome(@RequestParam("name") String name){

        logger.info("server-welcome:"+name);
        return "welcome:"+name;
    }


}
