package com.gagc.zookeeper.service;

import java.util.List;

/**
 * create by lorne on 2018/1/29
 */
public interface LogService {
    boolean set(String name);

     

    List<String> logs();

    boolean setVal(String name, String val);

    boolean del(String name);
}
