package com.gagc.zookeeper;

import org.I0Itec.zkclient.ZkClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * create by lorne on 2018/1/29
 */
@Configuration
public class ZkClientConfiguration {


    @Value("${zookeeper.host}")
    private String host;

    @Value("${zookeeper.port}")
    private int port;

    @Bean
    public ZkClient zkClient(){
        ZkClient zkClient = new ZkClient(host,port);
        return zkClient;
    }
}
