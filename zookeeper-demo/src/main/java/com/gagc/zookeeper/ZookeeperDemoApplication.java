package com.gagc.zookeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * create by lorne on 2018/1/29
 */
@SpringBootApplication
public class ZookeeperDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZookeeperDemoApplication.class,args);
    }

}
