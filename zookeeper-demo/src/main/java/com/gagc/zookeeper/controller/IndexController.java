package com.gagc.zookeeper.controller;

import com.gagc.zookeeper.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * create by lorne on 2018/1/29
 */
@RestController
public class IndexController {


    @Autowired
    private LogService logService;

    @RequestMapping("")
    public String index(){
        return "welcome";
    }


    @RequestMapping("/set")
    public boolean set(String name){
        return logService.set(name);
    }

    @RequestMapping("/del")
    public boolean del(String name){
        return logService.del(name);
    }

    @RequestMapping("/setVal")
    public boolean setVal(String name,String val){
        return logService.setVal(name,val);
    }

    @RequestMapping("/logs")
    public List<String> logs(){
        return logService.logs();
    }

}
